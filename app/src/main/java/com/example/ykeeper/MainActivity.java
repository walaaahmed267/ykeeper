package com.example.ykeeper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.amirarcane.lockscreen.activity.EnterPinActivity;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private final static int REQUEST_PIN_CODE = 4100;
    CountDownTimer countDownTimer;
    int Max_Time=40000;
    int Min_Time=1000;
    Boolean pin_flag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pin_flag=getIntent().getBooleanExtra("pin_flag",false);
        CardView cardView_Card = findViewById(R.id.card_id);
        LinearLayout main_LinearLayout=findViewById(R.id.main_liner);
        CardView cardView_Password = findViewById(R.id.password_id);








        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    /*    main_LinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){

                    // Do what you want
                    return true;
                }
                return false;
            }
        });*/




        Intent intent = new Intent(this, EnterPinActivity.class);
        startActivityForResult(intent, REQUEST_PIN_CODE);
        cardView_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //issue Walaa
                try {
                    countDownTimer.cancel();

                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent intent1 = new Intent(MainActivity.this, CardsActivity.class);
                startActivity(intent1);
            }
        });
        cardView_Password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    countDownTimer.cancel();

                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent intent2 = new Intent(MainActivity.this, DBPasswordsListActivity.class);
                startActivity(intent2);
            }
        });// change 1



       /* try {
            new Timer().schedule(new TimerTask(){
                public void run() {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);


                }//60 seconds60000
            }, 10000 );

        }catch (Exception e){
            e.printStackTrace();
        }


*/
    }


    public static void setLocale(Activity activity, String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Resources resources = activity.getResources();
        Configuration config = resources.getConfiguration();
        config.setLocale(locale);
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PIN_CODE:
                if (resultCode == EnterPinActivity.RESULT_BACK_PRESSED) {
                    //ew ewfinish();
                    finishAffinity();
                    System.exit(0);
                } else if (resultCode == RESULT_OK) {
                    if (pin_flag){
                        onBackPressed();
                    }else {


                        countDownTimer = new CountDownTimer(Max_Time, Min_Time) {

                            public void onTick(long millisUntilFinished) {
                                //TODO: Do something every second

                            }

                            public void onFinish() {

                                finishAffinity();
                                System.exit(0);
                            /*finishAffinity();
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);*/
                            }
                        }.start();
                    }


                }else {

                }
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            countDownTimer.cancel();
            countDownTimer.start();
        }
        return super.onTouchEvent(event);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            countDownTimer.cancel();
            finishAffinity();
            System.exit(0);
        }
        return super.onKeyDown(keyCode, event);
    }



}