package com.example.ykeeper;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PasswordDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_ID = "com.example.roomm.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.example.roomm.EXTRA_TITLE";
    public static final String EXTRA_NAME = "com.example.roomm.EXTRA_NAME";
    public static final String EXTRA_USER_ID = "com.example.roomm.EXTRA_USER_ID";
    public static final String EXTRA_PASSWORD = "com.example.roomm.EXTRA_PASSWORD";
    public static final String EXTRA_NOTES = "com.example.roomm.EXTRA_NOTES";

    int Max_Time=40000;
    int Min_Time=1000;



    private TextView editTextTitle;
    private TextView editTextName;
    private TextView editTextUser_ID;
    private TextView editTextPassword;
    private TextView editTextNotes;
    String Title;
    String Name ,User_Id;
    String Password ,Notes;
    CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_details);
        editTextTitle = findViewById(R.id.add_title);
        editTextName = findViewById(R.id.add_name);
        editTextUser_ID = findViewById(R.id.add_user_id);
        editTextPassword = findViewById(R.id.add_password);
        editTextNotes = findViewById(R.id.add_notes);
        // enabling action bar app icon and behaving it as toggle button
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back);

        //count timer
        countDownTimer = new CountDownTimer(Max_Time, Min_Time) {

            public void onTick(long millisUntilFinished) {
                //TODO: Do something every second

            }

            public void onFinish() {

                Intent intent=new Intent(PasswordDetailsActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);

//                finishAffinity();
//                startActivity(new Intent(PasswordDetailsActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/

            }
        }.start();


        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle(getResources().getString(R.string.detail_pass));
           /* editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextName.setText(intent.getStringExtra(EXTRA_NAME));
            editTextUser_ID.setText(intent.getStringExtra(EXTRA_USER_ID));
            editTextPassword.setText(intent.getStringExtra(EXTRA_PASSWORD));
            editTextNotes.setText(intent.getStringExtra(EXTRA_NOTES));*/

            Title=intent.getStringExtra(EXTRA_TITLE);
            Name=intent.getStringExtra(EXTRA_NAME);
            User_Id=intent.getStringExtra(EXTRA_USER_ID);
            Password=intent.getStringExtra(EXTRA_PASSWORD);
            Notes=intent.getStringExtra(EXTRA_NOTES);

        }
        //weeeeeeeeeeeeee
        this.customSimpleAdapterListView();


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                countDownTimer.cancel();
                Intent intent=new Intent(PasswordDetailsActivity.this,DBPasswordsListActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // This method shows how to customize SimpleAdapter to show image and text in ListView.
    private void customSimpleAdapterListView()
    {
        String[] titleArr = { getResources().getString(R.string.titlee), getResources().getString(R.string.Name), getResources().getString(R.string.user_id),  getResources().getString(R.string.password), getResources().getString(R.string.notes)};
        String[] descArr = {Title, Name, User_Id, Password, Notes };

        ArrayList<Map<String,Object>> itemDataList = new ArrayList<Map<String,Object>>();;

        int titleLen = titleArr.length;
        for(int i =0; i < titleLen; i++) {
            Map<String,Object> listItemMap = new HashMap<String,Object>();
            listItemMap.put("imageId", R.mipmap.ic_launcher);
            listItemMap.put("title", titleArr[i]);
            listItemMap.put("description", descArr[i]);
            itemDataList.add(listItemMap);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(this,itemDataList,R.layout.item_list,
                new String[]{"title","description"},new int[]{R.id.userTitle, R.id.userDesc});

        ListView listView = (ListView)findViewById(R.id.listViewExample);
        listView.setAdapter(simpleAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                HashMap clickItemMap = (HashMap)clickItemObj;
                String itemTitle = (String)clickItemMap.get("title");
                String itemDescription = (String)clickItemMap.get("description");

            }
        });
        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return PasswordDetailsActivity.super.onTouchEvent(event);
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            countDownTimer.cancel();
            countDownTimer.start();
            return false;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
            countDownTimer.cancel();
            startActivity(new Intent(PasswordDetailsActivity.this,DBPasswordsListActivity.class));
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onResume() {

        countDownTimer.cancel();


        countDownTimer.start();

        super.onResume();
    }


}