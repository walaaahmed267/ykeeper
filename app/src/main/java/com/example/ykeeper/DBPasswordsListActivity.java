package com.example.ykeeper;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ykeeper.Adapters.PasswordsAdapter;
import com.example.ykeeper.DBPasswords.PasswordModel;
import com.example.ykeeper.DBPasswords.PasswordViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class DBPasswordsListActivity extends AppCompatActivity {

    private PasswordViewModel passwordViewModel;
    public static final int ADD_PASSWORDS_REQUEST =1;
    public static final int Edit_PASSWORDS_REQUEST =2;
    RelativeLayout passRelativeLayout;
    CountDownTimer countDownTimer;
    int Max_Time=40000;
    int Min_Time=1000;
    RecyclerView recyclerView;
    boolean adapterIntentFlag=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d_b_passwords_list);
        passRelativeLayout=findViewById(R.id.pass_list_layout);
        recyclerView=findViewById(R.id.recycleview);

        countDownTimer = new CountDownTimer(Max_Time, Min_Time)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent=new Intent(DBPasswordsListActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);

//                finishAffinity();
//                startActivity(new Intent(DBPasswordsListActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();




        FloatingActionButton addImagefloatingActionButton=findViewById(R.id.fab);

        addImagefloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cancel counter
                try {
                    countDownTimer.cancel();

                }catch (Exception e){
                    e.printStackTrace();
                }
                //
                Intent intent=new Intent(DBPasswordsListActivity.this, AddEditPasswordsActivity.class);
                startActivityForResult(intent, ADD_PASSWORDS_REQUEST);
            }
        });
        RecyclerView recyclerView=findViewById(R.id.recycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        PasswordsAdapter passwordsAdapter=new PasswordsAdapter(this);
        recyclerView.setAdapter(passwordsAdapter);

        passwordViewModel = ViewModelProviders.of(this).get(PasswordViewModel.class);
        passwordViewModel.getAllPasswords().observe(this, new Observer<List<PasswordModel>>() {
            @Override
            public void onChanged(List<PasswordModel> passwordModels) {
                //update recycleview
                //Toast.makeText(DBPhotoListActivity.this,"onChanged",Toast.LENGTH_SHORT).show();
                passwordsAdapter.submitList(passwordModels);

            }
        });


        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return DBPasswordsListActivity.super.onTouchEvent(event);
            }
        });







//        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
//                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
//            @Override
//            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
//                return false;
//            }
//
//            @Override
//            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//
//                passwordViewModel.delete(passwordsAdapter.getPasswordsAt(viewHolder.getAdapterPosition()));
//                Toast.makeText(DBPasswordsListActivity.this,getString(R.string.dlte_msg),Toast.LENGTH_SHORT).show();
//
//
//            }
//        }).attachToRecyclerView(recyclerView);
        passwordsAdapter.setOnItemClickListener(new PasswordsAdapter.onItemClickListener() {
            @Override
            public void onItemClick(PasswordModel passwordModel) {

                // cancel counter
                try {
                    countDownTimer.cancel();

                }catch (Exception e){
                    e.printStackTrace();
                }
                //
                Intent editIntent=new Intent(DBPasswordsListActivity.this, AddEditPasswordsActivity.class);
                editIntent.putExtra(AddEditPasswordsActivity.EXTRA_ID,passwordModel.getId());
                editIntent.putExtra(AddEditPasswordsActivity.EXTRA_TITLE,passwordModel.getTitle());
                editIntent.putExtra(AddEditPasswordsActivity.EXTRA_NAME,passwordModel.getName());
                editIntent.putExtra(AddEditPasswordsActivity.EXTRA_USER_ID,passwordModel.getUserid());
                editIntent.putExtra(AddEditPasswordsActivity.EXTRA_PASSWORD,passwordModel.getPassword());
                editIntent.putExtra(AddEditPasswordsActivity.EXTRA_NOTES,passwordModel.getNotes());

                startActivityForResult(editIntent, Edit_PASSWORDS_REQUEST);
            }
        });

        //try second
        passwordsAdapter.setOnonPassDetailsClick(new PasswordsAdapter.passDetailslistener() {
            @Override
            public void onPassDetailsClick(PasswordModel passwordModel) {
                // cancel counter
                try {
                    countDownTimer.cancel();

                }catch (Exception e){
                    e.printStackTrace();
                }
                //
                Intent detailsIntent=new Intent(DBPasswordsListActivity.this, PasswordDetailsActivity.class);
                detailsIntent.putExtra(PasswordDetailsActivity.EXTRA_ID,passwordModel.getId());
                detailsIntent.putExtra(PasswordDetailsActivity.EXTRA_TITLE,passwordModel.getTitle());
                detailsIntent.putExtra(PasswordDetailsActivity.EXTRA_NAME,passwordModel.getName());
                detailsIntent.putExtra(PasswordDetailsActivity.EXTRA_USER_ID,passwordModel.getUserid());
                detailsIntent.putExtra(PasswordDetailsActivity.EXTRA_PASSWORD,passwordModel.getPassword());
                detailsIntent.putExtra(PasswordDetailsActivity.EXTRA_NOTES,passwordModel.getNotes());
                startActivity(detailsIntent);

            }
        });

        // View Activity all
        passwordsAdapter.setOnItemTouchListener(new PasswordsAdapter.OnItemTouchListener() {
            @Override
            public boolean OnItemTouchListener(MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
                return false;
            }
        });





    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        countDownTimer.start();
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== ADD_PASSWORDS_REQUEST && resultCode==RESULT_OK){
            String title=data.getStringExtra(AddEditPasswordsActivity.EXTRA_TITLE);
            String name=data.getStringExtra(AddEditPasswordsActivity.EXTRA_NAME);
            String user_Id=data.getStringExtra(AddEditPasswordsActivity.EXTRA_USER_ID);
            String password=data.getStringExtra(AddEditPasswordsActivity.EXTRA_PASSWORD);
            String notes=data.getStringExtra(AddEditPasswordsActivity.EXTRA_NOTES);
            // convert to bitmap
            PasswordModel passwordModel=new PasswordModel(title,name,user_Id,password,notes);
            passwordViewModel.insert(passwordModel);
            Toast.makeText(this,R.string.datasave_msg,Toast.LENGTH_SHORT).show();
        }else if (requestCode== Edit_PASSWORDS_REQUEST && resultCode==RESULT_OK){
            int id =data.getIntExtra(AddEditImageActivity.EXTRA_ID,-1);
            if (id==-1){
                Toast.makeText(this,R.string.cant_updateedata,Toast.LENGTH_SHORT).show();
                return;
            }
            String title=data.getStringExtra(AddEditPasswordsActivity.EXTRA_TITLE);
            String name=data.getStringExtra(AddEditPasswordsActivity.EXTRA_NAME);
            String user_ID=data.getStringExtra(AddEditPasswordsActivity.EXTRA_USER_ID);
            String password=data.getStringExtra(AddEditPasswordsActivity.EXTRA_PASSWORD);
            String notes=data.getStringExtra(AddEditPasswordsActivity.EXTRA_NOTES);
            // convert to bitmap
            PasswordModel passwordModel=new PasswordModel(title,name,user_ID,password,notes);
            passwordModel.setId(id);
            passwordViewModel.update(passwordModel);
            Toast.makeText(this,R.string.updatePasswordData,Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this,R.string.cancelmsg,Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.deleteAll:
                countDownTimer.cancel();
                countDownTimer.start();
                passwordViewModel.deleteAllPasswords();
                Toast.makeText(this,R.string.AllDeleted,Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);


        }

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
            countDownTimer.cancel();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {

        countDownTimer.cancel();

        countDownTimer.start();

        super.onResume();
    }


}