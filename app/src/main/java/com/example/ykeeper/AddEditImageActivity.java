package com.example.ykeeper;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ykeeper.DB.ImageViewModel;
import com.example.ykeeper.DB.Images;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddEditImageActivity extends AppCompatActivity {


    public static final String EXTRA_TITLE = "com.example.roomm.EXTRA_TITLE";
    public static final String EXTRA_ID = "com.example.roomm.EXTRA_ID";
    public static final String EXTRA_IMAGE = "com.example.roomm.EXTRA_IMAGE";
    //added 7-4
    public static final String EXTRA_IMAGE_TWO = "com.example.roomm.EXTRA_IMAGE_TWO";
    private static int ID =-2;

    int card_number;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    boolean first_image_clickedFlage;
    boolean second_image_clickedFlage;
    boolean first_gallery_clickedFlage;
    boolean second_gallery_clickedFlage;


    int Max_Time = 90000;
    int Min_Time = 1000;
    CountDownTimer countDownTimer;


    private EditText editTextTitle;
    private ImageView imageView1;
    private ImageView imageView2;

    private Uri imageFilePath;
    private Bitmap imageTostore1;
    private Bitmap imageTostore;
    //added
    private Bitmap imageTostore2;

    RelativeLayout Rlt;
    String currentPhotoPath;
    private ImageViewModel imageViewModel;



    private static final int Pick_image_Request = 100;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_image:
                countDownTimer.cancel();
                saveImage();
                return true;
            case android.R.id.home:
                finish();
                countDownTimer.cancel();
                Intent intent = new Intent(AddEditImageActivity.this, DBPhotoListActivity.class);
                startActivity(intent);

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void saveImage() {
        try {
            //walaa try to

            if (!editTextTitle.getText().toString().isEmpty() && imageView1.getDrawable() != null && imageTostore1 != null && imageView2.getDrawable() != null) {
                if (imageTostore2 == null) {

                    imageTostore2 = getBitmap(R.drawable.ic_camera);

                }
                String title = editTextTitle.getText().toString();
                String image1 = encodeImage(imageTostore1);
                //added
                String image2 = encodeImage(imageTostore2);
                //changed
//                Images images = new Images(title, card_number,image1, image2);
                Intent data = new Intent();
                data.putExtra(EXTRA_TITLE, title);
                data.putExtra(EXTRA_IMAGE, image1);
                Images image = new Images(title, card_number, decode(image1), decode(image2));
                //added
                data.putExtra(EXTRA_IMAGE_TWO, image2);
                int id = getIntent().getIntExtra(EXTRA_ID, -1);
                if (id != -1) {
                    data.putExtra(EXTRA_ID, id);
                    image.setId(id);
                    imageViewModel.update(image);
                    Toast.makeText(AddEditImageActivity.this,R.string.updatedimg,Toast.LENGTH_SHORT).show();

                }else {
                    imageViewModel.insert(image);
                    Toast.makeText(AddEditImageActivity.this,R.string.saveimg_msg,Toast.LENGTH_SHORT).show();

                }
//                setResult(RESULT_OK, data);
                finish();
            } else {
                Toast.makeText(this, getString(R.string.selct_img_and_title), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private byte[] decode(String encodedString) {
        return Base64.decode(encodedString, Base64.DEFAULT);
    }


    public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_image);
        editTextTitle = findViewById(R.id.add_title);
        imageView1 = findViewById(R.id.add_image);
        imageView2 = findViewById(R.id.add_image2);

        ImageView takephotobtn = (ImageView) findViewById(R.id.add_image);
        ImageView takephotobtn2 = (ImageView) findViewById(R.id.add_image2);
        Rlt = (RelativeLayout) findViewById(R.id.img_layout);
        imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);


        // enabling action bar app icon and behaving it as toggle button
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);



        //retrive shared pref
        SharedPreferences sharedpreferences = getSharedPreferences("mypreference", Context.MODE_PRIVATE);
        card_number = sharedpreferences.getInt("card_number", -1);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle(getResources().getString(R.string.edit_img));
            //elwel
            ID=intent.getIntExtra(EXTRA_ID,-2);
            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));

            ImageViewModel imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);
            imageViewModel.getAllImages(card_number).observe(this, new Observer<List<Images>>() {
                        @Override
                        public void onChanged(List<Images> images) {
                            //update recycleview
                            //Toast.makeText(DBPhotoListActivity.this,"onChanged",Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < images.size(); i++) {
                                if (ID == images.get(i).getId()) {
                                    byte[] imageToEdit = images.get(i).getImage();
                                    byte[] imageToEdit2 = images.get(i).getImage_two();
                                    Bitmap bitmapImage = BitmapFactory.decodeByteArray(images.get(i).getImage(), 0, images.get(i).getImage().length);
                                    imageView1.setImageBitmap(bitmapImage);
                                    imageTostore1 = bitmapImage;
                                    Bitmap bitmapImage2 = BitmapFactory.decodeByteArray(images.get(i).getImage_two(), 0, images.get(i).getImage_two().length);
                                    imageView2.setImageBitmap(bitmapImage2);
                                    imageTostore2 = bitmapImage2;
                                }
                            }
                        }
                    });



                            //elwell
// welwel           byte[] imageToEdit = intent.getByteArrayExtra(EXTRA_IMAGE);
//            Bitmap bitmapImage = BitmapFactory.decodeByteArray(imageToEdit, 0, imageToEdit.length);
//            imageView1.setImageBitmap(bitmapImage);
//            imageTostore1 = bitmapImage; // walaa added
//            //added 7-4
//            byte[] imageToEdit2 = intent.getByteArrayExtra(EXTRA_IMAGE_TWO);
//            Bitmap bitmapImage2 = BitmapFactory.decodeByteArray(imageToEdit2, 0, imageToEdit2.length);
//            imageView2.setImageBitmap(bitmapImage2);
//            imageTostore2 = bitmapImage2; // walaa added


        } else {
            setTitle(getResources().getString(R.string.add_img));
        }

        // counter
        // counter

        countDownTimer = new CountDownTimer(Max_Time, Min_Time) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {


                Intent intent=new Intent(AddEditImageActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();
        //////////////////////////////////////////////////
        takephotobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                first_image_clickedFlage = true;
                takePhoto();


            }
        });

        takephotobtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                second_image_clickedFlage = true;
                takePhoto();


            }
        });

        editTextTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
                return false;
            }
        });
        takephotobtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
                return false;
            }
        });
        takephotobtn2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
                return false;
            }
        });


    }

    /******************************************************/
    /**
     * Alert dialog for capture or select from galley
     */

    private void takePhoto() {
        final CharSequence[] items = getResources().getStringArray(R.array.choose_whichCam);
        CharSequence item_0 = items[0];
        CharSequence item__1 = items[1];
        CharSequence item_2 = items[2];

      /*  final CharSequence[] items = {choose_whichCam[0]), "Choose from Gallery",
                "Cancel"};*/
        AlertDialog.Builder builder = new AlertDialog.Builder(AddEditImageActivity.this);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(item_0)) {
                requestStoragePermission(true);
            } else if (items[item].equals(item__1)) {
                requestStoragePermission(false);
            } else if (items[item].equals(item_2)) {
                if (first_image_clickedFlage == true) {
                    first_image_clickedFlage = false;
                }
                if (second_image_clickedFlage == true) {
                    second_image_clickedFlage = false;
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Requesting multiple permissions (storage and camera) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                cameraIntent();
                            } else {
                                galleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).withErrorListener(error -> Toast.makeText(getApplicationContext(), getString(R.string.err_msg), Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();


    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, REQUEST_CAMERA);
        dispatchTakePictureIntent();
    }

    private void onSelectFromGalleryResult(Intent data) throws IOException {


        if (data != null) {
            try {



                imageFilePath = data.getData();
                Bitmap imageTostore77 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageFilePath);

//                imageTostore = Bitmap.createScaledBitmap(imageTostore77, 120, 160, false);
//
                int nh = (int) (imageTostore77.getHeight() * (512.0 / imageTostore77.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(imageTostore77, 512, nh, true);
                imageTostore = (scaled);

                if (first_image_clickedFlage == true) {
                    imageTostore1 = imageTostore;
                    imageView1.setImageBitmap(imageTostore1);
                    first_image_clickedFlage = false;

                }
                if (second_image_clickedFlage == true) {
                    imageTostore2 = imageTostore;
                    imageView2.setImageBitmap(imageTostore2);
                    second_image_clickedFlage = false;
                }
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void onCaptureImageResult(Intent data) {
        imageTostore = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();


        if (first_image_clickedFlage == true) {
            imageTostore1 = imageTostore;
            imageView1.setImageBitmap(imageTostore1);
            first_image_clickedFlage = false;

        }
        if (second_image_clickedFlage == true) {
            imageTostore2 = imageTostore;
            imageView2.setImageBitmap(imageTostore2);
            second_image_clickedFlage = false;
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_CAMERA)

                handleBigCameraPhoto();


        }
    }


    /**************************************/
    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.ykeeper.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void setPic() {

        /* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        int targetW = imageView1.getWidth();
        int targetH = imageView1.getHeight();

        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        /* Figure out which way needs to be reduced less */
        int scaleFactor = 2;
        if ((targetW > 0) && (targetH > 0)) {
            scaleFactor = Math.max(photoW / targetW, photoH / targetH);
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Matrix matrix = new Matrix();
        matrix.postRotate(getRotation());

        /* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
//        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);

        /* Associate the Bitmap to the ImageView */
        if (first_image_clickedFlage == true) {
            imageTostore1 = bitmap;
            imageView1.setImageBitmap(imageTostore1);
            first_image_clickedFlage = false;

        }
        if (second_image_clickedFlage == true) {
            imageTostore2 = bitmap;
            imageView2.setImageBitmap(imageTostore2);
            second_image_clickedFlage = false;
        }
    }

    private float getRotation() {
        try {
            ExifInterface ei = new ExifInterface(currentPhotoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90f;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180f;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270f;
                default:
                    return 0f;
            }
        } catch (Exception e) {
            Log.e("Add Recipe", "getRotation", e);
            return 0f;
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private void handleBigCameraPhoto() {

        if (currentPhotoPath != null) {
            setPic();
            galleryAddPic();
        }
    }

    /******************************************************/

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            finish();
            countDownTimer.cancel();
            startActivity(new Intent(AddEditImageActivity.this, DBPhotoListActivity.class));
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        countDownTimer.cancel();
        countDownTimer.start();
        super.onResume();
    }
}