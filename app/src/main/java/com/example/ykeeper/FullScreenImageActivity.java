package com.example.ykeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ykeeper.DB.ImageViewModel;
import com.example.ykeeper.DB.Images;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class FullScreenImageActivity extends AppCompatActivity {
    public static final String EXTRA_TITLE= "com.example.roomm.EXTRA_TITLE";
    public static final String EXTRA_ID= "com.example.roomm.EXTRA_ID";
    public static final String EXTRA_CAED_NO= "com.example.roomm.EXTRA_CAED_NO";



    public static final String EXTRA_IMG1 = "com.example.roomm.EXTRA_Image_one";
    public static final String EXTRA_IMG2 = "com.example.roomm.EXTRA_Image_two";
    private TextView title;
    private  ImageView imageView1,imageView2;
    String imageToEdit1;
    String imageToEdit2;
    byte[] IMAGE1;
    byte[] IAMGE2;
    Bitmap bitmapImage1, bitmapImage2;
    String Title;
    int ID=-1;
    int CARD_ID;

    String IMG1,IMG2;

    CountDownTimer countDownTimer;
    int Max_Time=40000;
    int Min_Time=1000;
    LinearLayout linearLayout;
    byte[] imgae_byte1,imgae_byte2;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);
        title=(TextView) findViewById(R.id.add_title);
        imageView1 = (ImageView) findViewById(R.id.add_image);
        imageView2 = (ImageView) findViewById(R.id.add_image2);
        linearLayout=(LinearLayout)findViewById(R.id.line1);

        // enabling action bar app icon and behaving it as toggle button
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back);
        //
        countDownTimer = new CountDownTimer(Max_Time, Min_Time)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                Intent intent=new Intent(FullScreenImageActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);

//                finishAffinity();
//                startActivity(new Intent(FullScreenImageActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();




        //
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return false;
            }
        });


        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_TITLE)) {
            Title=intent.getStringExtra(EXTRA_TITLE);
            setTitle(Title);
            ID=intent.getIntExtra(EXTRA_ID,-1);
            CARD_ID=intent.getIntExtra(EXTRA_CAED_NO,-1);

            if (ID!=-1){
                ImageViewModel imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);
                imageViewModel.getAllImages(CARD_ID).observe(this, new Observer<List<Images>>() {
                    @Override
                    public void onChanged(List<Images> images) {
                        //update recycleview
                        //Toast.makeText(DBPhotoListActivity.this,"onChanged",Toast.LENGTH_SHORT).show();
                        for (int i=0;i<images.size();i++){
                            if (ID==images.get(i).getId()){
                                imgae_byte1 = images.get(i).getImage();
                                imgae_byte2 = images.get(i).getImage_two();
                                bitmapImage1 = BitmapFactory.decodeByteArray(images.get(i).getImage(), 0, images.get(i).getImage().length);
                                imageView1.setImageBitmap(bitmapImage1);
                                bitmapImage2 = BitmapFactory.decodeByteArray(images.get(i).getImage_two(), 0, images.get(i).getImage_two().length);
                                imageView2.setImageBitmap(bitmapImage2);


                            }
                        }

                    }
                });



            }

//            imageToEdit1 = intent.getStringExtra(EXTRA_IMG1);
//            IMAGE1=decode(imageToEdit1);
//
//            bitmapImage1 = BitmapFactory.decodeByteArray(IMAGE1, 0, IMAGE1.length);
//
//            imageView1.setImageBitmap(bitmapImage1);


//
//            imageToEdit2 = intent.getStringExtra(EXTRA_IMG2);
//            IAMGE2=decode(imageToEdit2);
//
//            bitmapImage2 = BitmapFactory.decodeByteArray(IAMGE2, 0, IAMGE2.length);
//
//            imageView2.setImageBitmap(bitmapImage2);
        }
        imageView1.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                countDownTimer.cancel();
                Intent intent1=new Intent(FullScreenImageActivity.this,PhotoFirstActivity.class);
                intent1.putExtra("img",1);
                intent1.putExtra(FullScreenImageActivity.EXTRA_IMG1,imgae_byte1);
                startActivity(intent1);
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countDownTimer.cancel();
                Intent intent1=new Intent(FullScreenImageActivity.this,PhotoFirstActivity.class);
                intent1.putExtra("img",2);
                intent1.putExtra(FullScreenImageActivity.EXTRA_IMG1,imgae_byte2);
                startActivity(intent1);
            }
        });






    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
            countDownTimer.cancel();
            startActivity(new Intent(FullScreenImageActivity.this,DBPhotoListActivity.class));
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menuu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                countDownTimer.cancel();
                Intent intent=new Intent(FullScreenImageActivity.this,DBPhotoListActivity.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);


        }

    }

    private byte[] decode(String encodedString) {
        return Base64.decode(encodedString, Base64.DEFAULT);
    }


    public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    @Override
    protected void onResume() {
        countDownTimer.cancel();
        countDownTimer.start();

        super.onResume();
    }






}