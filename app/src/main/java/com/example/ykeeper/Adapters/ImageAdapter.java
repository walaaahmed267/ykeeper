package com.example.ykeeper.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ykeeper.DB.ImageViewModel;
import com.example.ykeeper.DBPasswords.PasswordModel;
import com.example.ykeeper.DBPasswords.PasswordViewModel;
import com.example.ykeeper.FullScreenImageActivity;
import com.example.ykeeper.DB.Images;
import com.example.ykeeper.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ImageAdapter extends ListAdapter<Images, ImageAdapter.ImageHolder> {
    private Context context;

    SharedPreferences sharedpreferences;
    boolean empty_image=false;


    private ImageAdapter.OnItemTouchListener onItemTouchListener;




    //private List<Images> images=new ArrayList<>();
    private onItemClickListener listener;
    private ImageViewModel imageViewModel;
    private ImageAdapter.imageDetailslistener imageDetailslistener;



    public ImageAdapter(Context context) {
        super(DIFF_Callback);
        this.context=context;

        //swipe
        imageViewModel = ViewModelProviders.of((FragmentActivity) context).get(ImageViewModel.class);


    }
    private static final DiffUtil.ItemCallback<Images> DIFF_Callback=new DiffUtil.ItemCallback<Images>() {
        @Override
        public boolean areItemsTheSame(@NonNull Images oldItem, @NonNull Images newItem) {
            return oldItem.getId()==newItem.getId();
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull Images oldItem, @NonNull Images newItem) {
            return oldItem.getTitle().equals(newItem.getTitle())&& (oldItem.getImage().equals(newItem.getImage()));
        }
    };

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_row,parent,false);
        return new ImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, int position) {
        Images currentImage=getItem(position);
        String title=currentImage.getTitle();
        int id=currentImage.getId();
        String idd=String.valueOf(id);
        byte[] bitmapdataImg = currentImage.getImage();
        //second img
        byte[] bitmapdataImg2 = currentImage.getImage_two();




       Bitmap bitmap=BitmapFactory.decodeByteArray(bitmapdataImg,0,bitmapdataImg.length);
       //two
        Bitmap bitmap2=BitmapFactory.decodeByteArray(bitmapdataImg2,0,bitmapdataImg2.length);
        // try to save in string to compare sec img

        String image2 = encodeImage(bitmap2);

        Bitmap image_Drawable =getBitmap(R.drawable.ic_camera);
        String image2_Drawable = encodeImage(image_Drawable);



        String data=  title;
        holder.textViewTitle.setText(data);
        //holder.textViewID.setText(idd);
        holder.imageView.setImageBitmap(bitmap);

        holder.imageDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageDetailslistener!=null && position!=RecyclerView.NO_POSITION){
                    imageDetailslistener.onImageDetailsClick(getItem(position));

                }

            }
        });
        holder.share.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (onItemTouchListener!=null){
                    onItemTouchListener.OnItemTouchListener(motionEvent);
                }
                return false;
            }
        });
        holder.txtDelete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (onItemTouchListener!=null){
                    onItemTouchListener.OnItemTouchListener(motionEvent);
                }
                return false;
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //---Save bitmap to external cache directory---//
                //get cache directory
                File cachePath = new File(context.getExternalCacheDir(), "my_images/");
                cachePath.mkdirs();
                //second
                File cachePath2 = new File(context.getExternalCacheDir(), "my_images/");
                cachePath2.mkdirs();




                //create png file
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String fname = "Image_"+ timeStamp +".jpg";

                //second

                String fname2 = "Image2_"+ timeStamp +".jpg";


                File file = new File(cachePath, fname);
                //second
                File file2 = new File(cachePath2, fname2);



                FileOutputStream fileOutputStream;
                //second
                FileOutputStream fileOutputStream2;
                try
                {
                    fileOutputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);

                    //second
                    fileOutputStream2 = new FileOutputStream(file2);

                    bitmap2.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream2);


                    fileOutputStream.flush();
                    fileOutputStream.close();
                    //second
                    fileOutputStream2.flush();
                    fileOutputStream2.close();

                } catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }

                //---Share File---//
                //get file uri
                Uri myImageFileUri = FileProvider.getUriForFile(context, "com.example.ykeeper.provider", file);
                //second
                Uri myImageFileUri2 = FileProvider.getUriForFile(context, "com.example.ykeeper.provider", file2);

/*

                //create a intent
                Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_STREAM, myImageFileUri);
                intent.putExtra(Intent.EXTRA_STREAM, myImageFileUri2);
                intent.setType("image/png");
                context.startActivity(Intent.createChooser(intent, "Share with"));*/

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Here are some files.");
                intent.setType("image/jpeg"); /* This example is sharing jpeg images. */

                ArrayList<Uri> files = new ArrayList<Uri>();
                files.add(myImageFileUri);
                if (image2.contentEquals(image2_Drawable)){


                }else {
                    files.add(myImageFileUri2);

                }
                intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
                context.startActivity(intent);


            }
        });
        //swipe

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //passwordsAdapter.getPasswordsAt(viewHolder.getAdapterPosition()))
                imageViewModel.delete(currentImage);
                Toast.makeText(context,R.string.deleted,Toast.LENGTH_SHORT).show();
            }
        });

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener!=null && position!=RecyclerView.NO_POSITION){
                    listener.onItemClick(getItem(position));

                }
            }
        });
    }


    class ImageHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        //private TextView textViewID;
        private ImageView imageView;
        //swipe
        private TextView txtDelete;
        private TextView txtEdit;
        private LinearLayout imageDetailsLayout;

        Button share;
        public ImageHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle=itemView.findViewById(R.id.title);
            //textViewID=itemView.findViewById(R.id.id);
            imageView=itemView.findViewById(R.id.image);

            // swipe
            txtDelete=itemView.findViewById(R.id.txt_delete);
            txtEdit=itemView.findViewById(R.id.txt_edit);

            share = itemView.findViewById(R.id.btnShare);
            imageDetailsLayout=itemView.findViewById(R.id.imageLayout);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (listener!=null && position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(getItem(position));

                    }
                }
            });*/

        }
    }

    public Images getImageAt(int position){
        return getItem(position);
    }
    public  interface onItemClickListener {
        public void onItemClick(Images images);
    }
    public void setOnItemClickListener(onItemClickListener listener){
        this.listener=listener;

    }

    // image details
    public  interface imageDetailslistener {
        public void onImageDetailsClick(Images images);

    }
    public void setOnImageDetailsClick(ImageAdapter.imageDetailslistener listener){
        this.imageDetailslistener=listener;

    }



    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = context.getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public  interface OnItemTouchListener   {
        public boolean OnItemTouchListener (MotionEvent event);
    }
    public  boolean setOnItemTouchListener(ImageAdapter.OnItemTouchListener  listener ){
        this.onItemTouchListener=listener;
        return false;
    }

    public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }




    ///////////////////////////////////////


}




