package com.example.ykeeper.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.ykeeper.DBPasswords.PasswordModel;
import com.example.ykeeper.Model.Cards;
import com.example.ykeeper.DBPhotoListActivity;
import com.example.ykeeper.R;

import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.cardHolder> {

    List<Cards> cardList;
    Context mContext;
    private CardAdapter.onItemClickListener listener;


    public CardAdapter(List<Cards> cards,Context mContext) {
        this.mContext=mContext;
        this.cardList = cards;
    }

    @Override
    public cardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        return new cardHolder(view);
    }

    @Override
    public void onBindViewHolder(cardHolder holder, int position) {
        int mPosition=position;
        LinearLayout parentLayout;
        Cards basicCards = cardList.get(position);
        holder.cardName.setText(basicCards.getName());
        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "this number   "+mPosition, Toast.LENGTH_SHORT).show();
                // put in shared preference
                SharedPreferences prefs = mContext.getSharedPreferences("mypreference", 0);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("card_number", mPosition); // Storing card id
                editor.apply();

                Intent intent = new Intent(mContext, DBPhotoListActivity.class);
                mContext.startActivity(intent);
            }
        });*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener!=null && position!=RecyclerView.NO_POSITION){
                    //Toast.makeText(mContext, "this number   "+mPosition, Toast.LENGTH_SHORT).show();
                    // put in shared preference
                    SharedPreferences prefs = mContext.getSharedPreferences("mypreference", 0);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt("card_number", mPosition); // Storing card id
                    editor.apply();
                    listener.onItemClick(mPosition);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    public class cardHolder extends RecyclerView.ViewHolder {
        TextView cardName;
        ImageView cardImage;
        TextView cardDetails;

        public cardHolder(View itemView) {
            super(itemView);
            cardName = itemView.findViewById(R.id.card_name);
            cardDetails = itemView.findViewById(R.id.card_details);
            cardImage = itemView.findViewById(R.id.cardImage);

        }
    }

    public  interface onItemClickListener {
        public void onItemClick(int  pos);
    }
    public void setOnItemClickListener(CardAdapter.onItemClickListener listener){
        this.listener=listener;

    }
}
