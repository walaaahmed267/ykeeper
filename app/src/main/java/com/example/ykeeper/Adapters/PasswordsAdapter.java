package com.example.ykeeper.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ykeeper.DBPasswords.PasswordModel;
import com.example.ykeeper.DBPasswords.PasswordViewModel;
import com.example.ykeeper.DBPasswordsListActivity;
import com.example.ykeeper.PasswordDetailsActivity;
import com.example.ykeeper.R;

public class PasswordsAdapter extends ListAdapter<PasswordModel, PasswordsAdapter.PasswordsHolder>  {
    private Context context;
    private PasswordViewModel passwordViewModel;
    PasswordModel passwordModelSecond;
    CountDownTimer countDownTimer;
    boolean intentFlag=false;


    //private List<Images> images=new ArrayList<>();
    private PasswordsAdapter.onItemClickListener listener;

    //try
    private PasswordsAdapter.passDetailslistener passDetailslistener;

    private PasswordsAdapter.OnItemTouchListener onItemTouchListener;


    public PasswordsAdapter(Context context) {
        super(DIFF_Callback);
        this.context=context;
        //Re wrote Ahmed from pass Activity to context
        //Re place view model crashed
        /*passwordViewModel = new ViewModelProvider((ViewModelStoreOwner) this,
                new ViewModelProvider.AndroidViewModelFactory(context.getApplication())).get(PasswordViewModel.class);*/
        passwordViewModel = ViewModelProviders.of((FragmentActivity) context).get(PasswordViewModel.class);


    }
    private static final DiffUtil.ItemCallback<PasswordModel> DIFF_Callback=new DiffUtil.ItemCallback<PasswordModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull PasswordModel oldItem, @NonNull PasswordModel newItem) {
            return oldItem.getId()==newItem.getId();
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull PasswordModel oldItem, @NonNull PasswordModel newItem) {
            return oldItem.getTitle().equals(newItem.getTitle())&& (oldItem.getName().equals(newItem.getName()))&& (oldItem.getUserid().equals(newItem.getUserid()))
                    && (oldItem.getPassword().equals(newItem.getPassword()))&& (oldItem.getNotes().equals(newItem.getNotes()));
        }
    };

    @NonNull
    @Override
    public PasswordsAdapter.PasswordsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.password_item_list,parent,false);
        return new PasswordsAdapter.PasswordsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PasswordsHolder holder, int position) {
        PasswordModel passwordModel=getItem(position);
        String title=passwordModel.getTitle();
        String name=passwordModel.getName();
        String user_id=passwordModel.getUserid();
        String password=passwordModel.getPassword();
        String notes=passwordModel.getNotes();
        holder.textViewTitle.setText(title);
        holder.textViewUserName.setText(name);

           holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //passwordsAdapter.getPasswordsAt(viewHolder.getAdapterPosition()))
                passwordViewModel.delete(passwordModel);
                Toast.makeText(context,R.string.deleted,Toast.LENGTH_SHORT).show();
            }
        });

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener!=null && position!=RecyclerView.NO_POSITION){
                    listener.onItemClick(getItem(position));

                }
            }
        });
        holder.passDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passDetailslistener!=null && position!=RecyclerView.NO_POSITION){
                    passDetailslistener.onPassDetailsClick(getItem(position));

                }

            }
        });



        holder.txtDelete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (onItemTouchListener!=null){
                    onItemTouchListener.OnItemTouchListener(event);
                }
                return false;
            }
        });




    }




    class PasswordsHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewUserName;
        private TextView txtDelete;
        private TextView txtEdit;
        private ImageView imageView;
        private ImageView editImageView;
        private ImageView deleteImageView;
        private LinearLayout passDetails;
        private  RecyclerView Rec;

        //private TextView textViewID;
        public PasswordsHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle=itemView.findViewById(R.id._title);
            textViewUserName=itemView.findViewById(R.id.user_name);
            imageView=itemView.findViewById(R.id.dumImage);
            txtDelete=itemView.findViewById(R.id.txt_delete);
            txtEdit=itemView.findViewById(R.id.txt_edit);
            passDetails = itemView.findViewById(R.id.passwordLayout);



        }
    }

    public PasswordModel getPasswordsAt(int position){
        return getItem(position);
    }


    public  interface onItemClickListener {
        public void onItemClick(PasswordModel passwordModel);
    }
    public void setOnItemClickListener(PasswordsAdapter.onItemClickListener listener){
        this.listener=listener;

    }



    //try4
    public  interface passDetailslistener {
        public void onPassDetailsClick(PasswordModel passwordModel);

    }
    public void setOnonPassDetailsClick(PasswordsAdapter.passDetailslistener listener){
        this.passDetailslistener=listener;

    }
    ///////////////////////////////////////

    public  interface OnItemTouchListener   {
        public boolean OnItemTouchListener (MotionEvent event);
    }
    public  boolean setOnItemTouchListener(PasswordsAdapter.OnItemTouchListener  listener ){
        this.onItemTouchListener=listener;
        return false;
    }


}




