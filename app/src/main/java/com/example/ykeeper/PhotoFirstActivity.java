package com.example.ykeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class PhotoFirstActivity extends AppCompatActivity {
    public static final String EXTRA_IMG1 = "com.example.roomm.EXTRA_Image_one";
    public static final String EXTRA_IMG2 = "com.example.roomm.EXTRA_Image_two";

    public static final String EXTRA_TITLE = "com.example.roomm.EXTRA_ID";

    ImageView imageView1;
    byte[] imageToShow1,imageToShow2;
    Bitmap bitmapImageToShow1,bitmapImageToShow2,bitmapImageToShow;
    CountDownTimer countDownTimer;
    Bitmap imageToEdit1,imageToEdit2;
    String Title;
    int img;
    int Max_Time = 40000;
    int Min_Time = 1000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_first);
        // enabling action bar app icon and behaving it as toggle button
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back);



        imageView1 = (ImageView) findViewById(R.id.img1);

        //
        countDownTimer = new CountDownTimer(Max_Time, Min_Time)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                Intent intent=new Intent(PhotoFirstActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);

//                finishAffinity();
//                startActivity(new Intent(PhotoFirstActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();


        Intent intent = getIntent();
        if (intent.hasExtra("img")) {
            imageToShow1 = intent.getByteArrayExtra(EXTRA_IMG1);
            // walaa wrong here
            bitmapImageToShow1 = BitmapFactory.decodeByteArray(imageToShow1, 0, imageToShow1.length);
            imageView1.setImageBitmap(bitmapImageToShow1);

//            int nh = (int) (bitmapImageToShow.getHeight() * (512.0 / bitmapImageToShow.getWidth()));
//            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImageToShow, 512, nh, true);
//            imageView1.setImageBitmap(scaled);
        }
        //setTitle(getResources().getString(R.string.detail_card));
        imageView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return false;
            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            countDownTimer.cancel();
            finish();
//            Intent intent = new Intent(PhotoFirstActivity.this, FullScreenImageActivity.class);
//            intent.putExtra(FullScreenImageActivity.EXTRA_TITLE,Title);
//            intent.putExtra(FullScreenImageActivity.EXTRA_IMG1,imageToEdit1);
//            intent.putExtra(FullScreenImageActivity.EXTRA_IMG2,imageToEdit2);
//            startActivity(intent);

        }
        return super.onKeyDown(keyCode, event);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                countDownTimer.cancel();
                finish();

                Intent intent = new Intent(PhotoFirstActivity.this, DBPhotoListActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onResume() {
        countDownTimer.cancel();
        countDownTimer.start();

        super.onResume();
    }



}