package com.example.ykeeper.DB;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.ykeeper.DBPasswords.PasswordDao;
import com.example.ykeeper.DBPasswords.PasswordModel;


@Database(entities = {Images.class,PasswordModel.class},version = 7)
public abstract class ImageDataBase  extends RoomDatabase {
    private static ImageDataBase instance;
    public abstract ImageDao imageDao();
    public abstract PasswordDao passwordDao();

    public static synchronized ImageDataBase getInstance(Context context){
        if (instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),
                    ImageDataBase.class,"image_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }
    private static Callback roomCallback = new Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };
}
