package com.example.ykeeper.DB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "image_table")
public class Images {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "title")
    private String title;

    public int getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(int cardnumber) {
        this.cardnumber = cardnumber;
    }

    @ColumnInfo(name = "cardnumber")
    private int cardnumber;

    @ColumnInfo(name = "image", typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    //added 4/7
    @ColumnInfo(name = "image_two", typeAffinity = ColumnInfo.BLOB)
    private byte[] image_two;

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    //added
    public void setImage_two(byte[] image_two) {
        this.image_two = image_two;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public byte[] getImage() {
        return image;
    }
    //added
    public byte[] getImage_two() {
        return image_two;
    }


    public Images(String title,int cardnumber, byte[] image, byte[] image_two) {
        this.title = title;
        this.cardnumber=cardnumber;
        this.image=image;
        //added
        this.image_two=image_two;
    }


}
