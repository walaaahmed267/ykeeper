package com.example.ykeeper.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;

@Dao
public interface ImageDao {
    @Insert
    void insert(Images images);
    @Update
    void update(Images images);
    @Delete
    void delete(Images images);


    @Query("Delete From image_table")
    void deleteAllImages();

    @Query("SELECT * FROM image_table  WHERE cardnumber=:x ORDER BY id ASC")
    LiveData<List<Images>> getAllImages(int x);

    @Query("DELETE FROM image_table WHERE cardnumber=:cardnumber")
    void deleteAllWithCardId(int  cardnumber);





}
