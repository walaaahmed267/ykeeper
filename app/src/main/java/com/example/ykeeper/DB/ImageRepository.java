package com.example.ykeeper.DB;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ImageRepository {
    private ImageDao imageDao;
    private LiveData<List<Images>> allImages;

    public ImageRepository(Application application){
        ImageDataBase dataBase= ImageDataBase.getInstance(application);
        imageDao=dataBase.imageDao();
        //e
    }
    public void insert(Images images){
        new InsertImageAsyncTask(imageDao).execute(images);

    }
    public void update(Images images){
        new UpdateImageAsyncTask(imageDao).execute(images);


    }
    public void delete(Images images){
        new DeleteImageAsyncTask(imageDao).execute(images);


    }
    public void deleteAllImages(){
        new DeletAlleImageAsyncTask(imageDao).execute();


    }
    public LiveData<List<Images>> getAllImages(int x){
        return imageDao.getAllImages(x);
    }

    public void deleteAllWithCardId(int  cardnumber){
        new DeletAlleImageWithCardIdAsyncTask(imageDao,cardnumber).execute();


    }

    private static class InsertImageAsyncTask extends AsyncTask<Images,Void,Void>{
        private ImageDao imageDao;
        private InsertImageAsyncTask(ImageDao imageDao){
            this.imageDao=imageDao;
        }
        @Override
        protected Void doInBackground(Images... images) {
            imageDao.insert(images[0]);
            return null;
        }
    }
    private static class UpdateImageAsyncTask extends AsyncTask<Images,Void,Void>{
        private ImageDao imageDao;
        private UpdateImageAsyncTask(ImageDao imageDao){
            this.imageDao=imageDao;
        }
        @Override
        protected Void doInBackground(Images... images) {
            imageDao.update(images[0]);
            return null;
        }
    }
    private static class DeleteImageAsyncTask extends AsyncTask<Images,Void,Void>{
        private ImageDao imageDao;
        private DeleteImageAsyncTask(ImageDao imageDao){
            this.imageDao=imageDao;
        }
        @Override
        protected Void doInBackground(Images... images) {
            imageDao.delete(images[0]);
            return null;
        }
    }


    private static class DeletAlleImageAsyncTask extends AsyncTask<Void,Void,Void>{
        private ImageDao imageDao;
        private DeletAlleImageAsyncTask(ImageDao imageDao){
            this.imageDao=imageDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            imageDao.deleteAllImages();
            return null;
        }
    }


    private static class DeletAlleImageWithCardIdAsyncTask extends AsyncTask<Void,Void,Void>{
        private ImageDao imageDao;
        private  int cardnumber;
        private DeletAlleImageWithCardIdAsyncTask(ImageDao imageDao, int cardnumber){
            this.imageDao=imageDao;
            this.cardnumber=cardnumber;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            imageDao.deleteAllWithCardId(cardnumber);
            return null;
        }
    }




}
