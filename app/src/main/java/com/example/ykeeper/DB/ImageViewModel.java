package com.example.ykeeper.DB;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ImageViewModel extends AndroidViewModel {
    private ImageRepository repository;
    private LiveData<List<Images>> allImages;

    public ImageViewModel(@NonNull Application application) {
        super(application);
        repository=new ImageRepository(application);

    }
    public void insert(Images images){
        repository.insert(images);
    }
    public void update(Images images){
        repository.update(images);
    }
    public void delete(Images images){
        repository.delete(images);
    }
    public void deleteAllImages(){
        repository.deleteAllImages();
    }
    public LiveData<List<Images>> getAllImages(int card_id ){
        return repository.getAllImages(card_id);
    }
    public void deleteAllImagesByCard(int card_id){
        repository.deleteAllWithCardId(card_id);
    }

}
