package com.example.ykeeper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;

import com.example.ykeeper.Adapters.CardAdapter;
import com.example.ykeeper.Model.Cards;

import java.util.ArrayList;
import java.util.List;

public class CardsActivity extends AppCompatActivity {
    private static RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    int Max_Time=40000;
    int Min_Time=1000;
    CountDownTimer countDownTimer;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);
        String [] cards_names ;
        cards_names = getResources().getStringArray(R.array.cards_names);
        List<Cards> cards = new ArrayList<>();
        CardAdapter cardAdapter;



        /* <----------------DUMMY DATA ------------->*/
        for (int i=0;i<=10;i++){
            cards.add(new Cards(cards_names[i]));
        }
       /* cards.add(new Cards());
        cards.add(new Cards("Passport "));
        cards.add(new Cards("Driving License "));
        cards.add(new Cards("certificates "));
        cards.add(new Cards("Business card "));
        cards.add(new Cards("Debit Card "));
        cards.add(new Cards("Insurance Card "));
        cards.add(new Cards("Office ID Card "));
        cards.add(new Cards("Personal Photo "));
        cards.add(new Cards("Birthday Certificate "));
        cards.add(new Cards("Others "));*/

        cardAdapter = new CardAdapter(cards,this);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cardAdapter);
        // counter
        // Counter
        countDownTimer = new CountDownTimer(Max_Time, Min_Time) {

            public void onTick(long millisUntilFinished) {
                //TODO: Do something every second

            }

            public void onFinish() {
                Intent intent=new Intent(CardsActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);
//                finishAffinity();
//                startActivity(new Intent(CardsActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();


        cardAdapter.setOnItemClickListener(new CardAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                countDownTimer.cancel();
                Intent intent = new Intent(CardsActivity.this, DBPhotoListActivity.class);
                startActivity(intent);
            }
        });





    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();

            countDownTimer.cancel();
            startActivity(new Intent(CardsActivity.this,MainActivity.class));
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onResume() {
        countDownTimer.cancel();
        countDownTimer.start();

        super.onResume();
    }



}
