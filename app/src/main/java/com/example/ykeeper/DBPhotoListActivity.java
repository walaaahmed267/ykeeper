package com.example.ykeeper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.ykeeper.Adapters.ImageAdapter;
import com.example.ykeeper.Adapters.PasswordsAdapter;
import com.example.ykeeper.DB.ImageViewModel;
import com.example.ykeeper.DB.Images;
import com.example.ykeeper.DBPasswords.PasswordViewModel;
import com.example.ykeeper.Model.Cards;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class DBPhotoListActivity extends AppCompatActivity {

    private ImageViewModel imageViewModel;
    public static final int ADD_IMAGE_REQUEST = 1;
    public static final int Edit_IMAGE_REQUEST = 2;
    int card_number;
    CountDownTimer countDownTimer;
    int Max_Time=40000;
    int Min_Time=1000;
    String [] cards_names ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d_b_photo_list);




        //retrive shared pref
        SharedPreferences sharedpreferences = getSharedPreferences("mypreference", Context.MODE_PRIVATE);
        card_number = sharedpreferences.getInt("card_number", -1);
        ///

        //titling
        cards_names = getResources().getStringArray(R.array.cards_names);
        setTitle(cards_names[card_number]);

        //counter
        //
        countDownTimer = new CountDownTimer(Max_Time, Min_Time)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                Intent intent=new Intent(DBPhotoListActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);

//                finishAffinity();
//                startActivity(new Intent(DBPhotoListActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();
        //////////////////



        FloatingActionButton addImagefloatingActionButton = findViewById(R.id.fab);
        addImagefloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cancel counter
                countDownTimer.cancel();
                //
                Intent intent = new Intent(DBPhotoListActivity.this, AddEditImageActivity.class);
                startActivityForResult(intent, ADD_IMAGE_REQUEST);
            }
        });
        RecyclerView recyclerView = findViewById(R.id.recycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        ImageAdapter imageAdapter = new ImageAdapter(this);
        recyclerView.setAdapter(imageAdapter);

        imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);
        imageViewModel.getAllImages(card_number).observe(this, new Observer<List<Images>>() {
            @Override
            public void onChanged(List<Images> images) {
                //update recycleview
                //Toast.makeText(DBPhotoListActivity.this,"onChanged",Toast.LENGTH_SHORT).show();
                imageAdapter.submitList(images);

            }
        });


        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return DBPhotoListActivity.super.onTouchEvent(event);
            }
        });


        /*new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                imageViewModel.delete(imageAdapter.getImageAt(viewHolder.getAdapterPosition()));
                Toast.makeText(DBPhotoListActivity.this, getString(R.string.dlte_msg), Toast.LENGTH_SHORT).show();


            }
        }).attachToRecyclerView(recyclerView);*/
        imageAdapter.setOnItemClickListener(new ImageAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Images images) {
                // cancel counter
                countDownTimer.cancel();
                //
                Intent editIntent = new Intent(DBPhotoListActivity.this, AddEditImageActivity.class);
                editIntent.putExtra(AddEditImageActivity.EXTRA_ID, images.getId());
                editIntent.putExtra(AddEditImageActivity.EXTRA_TITLE, images.getTitle());
// WW                 editIntent.putExtra(AddEditImageActivity.EXTRA_IMAGE, images.getImage());
//  WW                editIntent.putExtra(AddEditImageActivity.EXTRA_IMAGE_TWO, images.getImage_two());

                startActivityForResult(editIntent, Edit_IMAGE_REQUEST);
            }
        });

        // image details
        imageAdapter.setOnImageDetailsClick(new ImageAdapter.imageDetailslistener() {
            @Override
            public void onImageDetailsClick(Images images) {
                // cancel counter
                countDownTimer.cancel();
                //

                try {


//                    Bitmap bitmap_img1 = BitmapFactory.decodeByteArray(images.getImage(), 0, images.getImage().length);
//                    Bitmap bitmap_img2 = BitmapFactory.decodeByteArray(images.getImage_two(), 0, images.getImage_two().length);


                    Intent detailsIntent = new Intent(DBPhotoListActivity.this, FullScreenImageActivity.class);
                    detailsIntent.putExtra(FullScreenImageActivity.EXTRA_TITLE, images.getTitle());
                    detailsIntent.putExtra(FullScreenImageActivity.EXTRA_ID, images.getId());
                    detailsIntent.putExtra(FullScreenImageActivity.EXTRA_CAED_NO, card_number);


                    startActivity(detailsIntent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        imageAdapter.setOnItemTouchListener(new ImageAdapter.OnItemTouchListener() {
            @Override
            public boolean OnItemTouchListener(MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
                return false;
            }
        });




    }

    private byte[] decode(String encodedString) {
        return Base64.decode(encodedString, Base64.DEFAULT);
    }



//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        countDownTimer.start();
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == ADD_IMAGE_REQUEST && resultCode == RESULT_OK) {
//            String title = data.getStringExtra(AddEditImageActivity.EXTRA_TITLE);
//            String image1 = data.getStringExtra(AddEditImageActivity.EXTRA_IMAGE);
//            String image2 = data.getStringExtra(AddEditImageActivity.EXTRA_IMAGE_TWO);
//
//            // convert to bitmap
////            Bitmap bitmapImage = BitmapFactory.decodeByteArray(decode(image1), 0, decode(image1).length);
//            // added 7-4
////            byte[] imageToAdd2 = data.getByteArrayExtra(AddEditImageActivity.EXTRA_IMAGE_TWO);
//            // convert to bitmap
////            Bitmap bitmapImage2 = BitmapFactory.decodeByteArray(imageToAdd2, 0, imageToAdd2.length);
//            //changed
//            Images image = new Images(title, card_number, decode(image1), decode(image2));
//            imageViewModel.insert(image);
//            Toast.makeText(this,R.string.saveimg_msg, Toast.LENGTH_SHORT).show();
//        } else if (requestCode == Edit_IMAGE_REQUEST && resultCode == RESULT_OK) {
//            int id = data.getIntExtra(AddEditImageActivity.EXTRA_ID, -1);
//            if (id == -1) {
//                Toast.makeText(this, "Image can't be updated ", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            String title = data.getStringExtra(AddEditImageActivity.EXTRA_TITLE);
//            String image1 = data.getStringExtra(AddEditImageActivity.EXTRA_IMAGE);
//            String image2 = data.getStringExtra(AddEditImageActivity.EXTRA_IMAGE_TWO);
//
//           /* byte[] imageToEdit = data.getByteArrayExtra(AddEditImageActivity.EXTRA_IMAGE);
//            // convert to bitmap
//            Bitmap bitmapImage = BitmapFactory.decodeByteArray(imageToEdit, 0, imageToEdit.length);
//            // added 7-4
//            byte[] imageToEdit2 = data.getByteArrayExtra(AddEditImageActivity.EXTRA_IMAGE_TWO);
//            // convert to bitmap
//            Bitmap bitmapImage2 = BitmapFactory.decodeByteArray(imageToEdit2, 0, imageToEdit2.length);*/
//            //changed
//            Images image = new Images(title, card_number, decode(image1), decode(image2));
//            image.setId(id);
//            imageViewModel.update(image);
//            Toast.makeText(this, "image updated ", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Canceled ", Toast.LENGTH_SHORT).show();
//
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteAll:
                countDownTimer.cancel();
                countDownTimer.start();
                imageViewModel.deleteAllImagesByCard(card_number);
                Toast.makeText(this,R.string.AllDeleted, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);


        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {

            countDownTimer.cancel();
            finish();
            startActivity(new Intent(DBPhotoListActivity.this,CardsActivity.class));
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onResume() {
        countDownTimer.cancel();
        countDownTimer.start();


        super.onResume();
    }






    public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }
}