package com.example.ykeeper.DBPasswords;


import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;


import com.example.ykeeper.DB.ImageDataBase;
import com.example.ykeeper.DB.ImageRepository;

import java.util.List;

public class PasswordsRepository {
    private PasswordDao passwordDao;
    private LiveData<List<PasswordModel>> allPasswords;

    public PasswordsRepository(Application application){
        ImageDataBase dataBase= ImageDataBase.getInstance(application);
        passwordDao=dataBase.passwordDao();
        //e
    }





    public void insert(PasswordModel passwordModel){
        new PasswordsRepository.InsertPasswordsAsyncTask(passwordDao).execute(passwordModel);

    }
    public void update(PasswordModel passwordModel){
        new PasswordsRepository.UpdatePasswordsAsyncTask(passwordDao).execute(passwordModel);


    }
    public void delete(PasswordModel passwordModel){
        new PasswordsRepository.DeletePasswordsAsyncTask(passwordDao).execute(passwordModel);


    }
    public void deleteAllPasswords(){
        new PasswordsRepository.DeletAllPasswordsAsyncTask(passwordDao).execute();


    }
    public LiveData<List<PasswordModel>> getAllPasswords(){
        return passwordDao.getAllPasswords();
    }

    private static class InsertPasswordsAsyncTask extends AsyncTask<PasswordModel,Void,Void>{
        private PasswordDao passwordDao;
        private InsertPasswordsAsyncTask(PasswordDao passwordDao){
            this.passwordDao=passwordDao;
        }
        @Override
        protected Void doInBackground(PasswordModel... passwordModels) {
            passwordDao.insert(passwordModels[0]);
            return null;
        }
    }
    private static class UpdatePasswordsAsyncTask extends AsyncTask<PasswordModel,Void,Void>{
        private PasswordDao passwordDao;
        private UpdatePasswordsAsyncTask(PasswordDao passwordDao){
            this.passwordDao=passwordDao;
        }
        @Override
        protected Void doInBackground(PasswordModel... passwordModels) {
            passwordDao.update(passwordModels[0]);
            return null;
        }
    }
    private static class DeletePasswordsAsyncTask extends AsyncTask<PasswordModel,Void,Void> {
        private PasswordDao passwordDao;
        private DeletePasswordsAsyncTask(PasswordDao passwordDao){
            this.passwordDao=passwordDao;
        }
        @Override
        protected Void doInBackground(PasswordModel... passwordModels) {
            passwordDao.delete(passwordModels[0]);
            return null;
        }
    }
    private static class DeletAllPasswordsAsyncTask extends AsyncTask<Void,Void,Void>{
        private PasswordDao passwordDao;
        private DeletAllPasswordsAsyncTask(PasswordDao passwordDao){
            this.passwordDao=passwordDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            passwordDao.deleteAllPasswords();
            return null;
        }
    }

}
