package com.example.ykeeper.DBPasswords;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import com.example.ykeeper.DBPasswords.PasswordModel;


import java.util.List;
@Dao
public interface PasswordDao {
    //Create Dao for Passwords
    @Insert
    void insert(PasswordModel passwordModel);
    @Update
    void update(PasswordModel passwordModel);
    @Delete
    void delete(PasswordModel passwordModel);

    @Query("Delete From PasswordModel_table")
    void deleteAllPasswords();

    @Query("SELECT * FROM PasswordModel_table  ORDER BY id ASC")
    LiveData<List<PasswordModel>> getAllPasswords();
}
