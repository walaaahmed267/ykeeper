package com.example.ykeeper.DBPasswords;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName ="PasswordModel_table")
    public class PasswordModel {
        @PrimaryKey(autoGenerate = true)
        private int id;
        @ColumnInfo(name = "title")
        private String title;

        @ColumnInfo(name = "name")
        private String name;

        @ColumnInfo(name = "userid")
        private String userid;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        @ColumnInfo(name = "password")
        private String password;

        @ColumnInfo(name = "notes")
        private String notes;

        public void setId(int id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }


        public PasswordModel(String title,String name,String userid,String password,String notes ) {
            this.title = title;
            this.name=name;
            this.userid=userid;
            this.password=password;
            this.notes=notes;
        }

    }
