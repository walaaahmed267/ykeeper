package com.example.ykeeper.DBPasswords;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


import java.util.List;

public class PasswordViewModel extends AndroidViewModel {
    private PasswordsRepository repository;
    private LiveData<List<PasswordModel>> allPasswords;

    public PasswordViewModel(@NonNull Application application) {
        super(application);
        repository=new PasswordsRepository(application);

    }
    public void insert(PasswordModel passwordModel){
        repository.insert(passwordModel);
    }
    public void update(PasswordModel passwordModel){
        repository.update(passwordModel);
    }
    public void delete(PasswordModel passwordModel){
        repository.delete(passwordModel);
    }
    public void deleteAllPasswords(){
        repository.deleteAllPasswords();
    }
    public LiveData<List<PasswordModel>> getAllPasswords(){
        return repository.getAllPasswords();
    }
}
