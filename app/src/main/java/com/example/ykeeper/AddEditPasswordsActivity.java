package com.example.ykeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ykeeper.DBPasswords.PasswordModel;
import com.example.ykeeper.DBPasswords.PasswordViewModel;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class AddEditPasswordsActivity extends AppCompatActivity {
    public static final String EXTRA_ID="com.example.roomm.EXTRA_ID";
    public static final String EXTRA_TITLE="com.example.roomm.EXTRA_TITLE";
    public static final String EXTRA_NAME="com.example.roomm.EXTRA_NAME";
    public static final String EXTRA_USER_ID="com.example.roomm.EXTRA_USER_ID";
    public static final String EXTRA_PASSWORD="com.example.roomm.EXTRA_PASSWORD";
    public static final String EXTRA_NOTES="com.example.roomm.EXTRA_NOTES";

    int Max_Time=40000;
    int Min_Time=1000;




    private EditText editTextTitle;
    private EditText editTextName;
    private EditText editTextUser_ID;
    private EditText editTextPassword;
    private EditText editTextNotes;
    CountDownTimer countDownTimer;
    RelativeLayout AllRelativeLayoutEditText;

    private PasswordViewModel passwordViewModel;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_image :
                countDownTimer.cancel();
                saveImage();
                return true;
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                countDownTimer.cancel();
                Intent intent=new Intent(AddEditPasswordsActivity.this,DBPasswordsListActivity.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void saveImage() {
        try {
            //walaa try to
            //&&!editTextName.getText().toString().isEmpty()
            //                    && !editTextUser_ID.getText().toString().isEmpty()&&
            //                    !editTextPassword.getText().toString().isEmpty()&& !editTextNotes.getText().toString().isEmpty()

            if (!editTextTitle.getText().toString().isEmpty() && !editTextName.getText().toString().isEmpty() && editTextPassword.getText().toString().length()!=0){
                String title=editTextTitle.getText().toString();
                String name=editTextName.getText().toString();
                String user_id=editTextUser_ID.getText().toString();
                String password=editTextPassword.getText().toString();
                String notes=editTextNotes.getText().toString();

                PasswordModel passwordModel=new PasswordModel(title,name,user_id,password,notes);
                Intent data=new Intent();
                data.putExtra(EXTRA_TITLE,title);
                data.putExtra(EXTRA_NAME,name);
                data.putExtra(EXTRA_USER_ID,user_id);
                data.putExtra(EXTRA_PASSWORD,password);
                data.putExtra(EXTRA_NOTES,notes);
                int id=getIntent().getIntExtra(EXTRA_ID,-1);
                if (id!=-1){
                    data.putExtra(EXTRA_ID,id);
                }
                setResult(RESULT_OK,data);
                finish();

            }else {


                if(editTextTitle.getText().toString().isEmpty()){
                    editTextTitle.setError("error");
                    editTextTitle.setBackgroundResource(R.drawable.error_background);
                    //editTextTitle.setBackgroundColor(getResources().getColor(R.color.errorColor));
                }else {
                    editTextTitle.setBackgroundResource(R.drawable.normal_background);

                }
                if(editTextName.getText().toString().isEmpty()){
                    editTextName.setError("error");
                    editTextName.setBackgroundResource(R.drawable.error_background);
                }else {
                    editTextName.setBackgroundResource(R.drawable.normal_background);

                }
                if(editTextPassword.getText().toString().isEmpty()){
                    editTextPassword.setError("error");
                    editTextPassword.setBackgroundResource(R.drawable.error_background);
                }else {
                    editTextPassword.setBackgroundResource(R.drawable.normal_background);

                }
                Toast.makeText(this,getString(R.string.selct_pass_name_and_title),Toast.LENGTH_SHORT).show();

            }
        }catch (Exception e){

            Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_passwords);
        editTextTitle=findViewById(R.id.add_title);
        editTextName=findViewById(R.id.add_name);
        editTextUser_ID=findViewById(R.id.add_user_id);
        editTextPassword=findViewById(R.id.add_password);
        editTextNotes=findViewById(R.id.add_notes);

        //counter
        countDownTimer = new CountDownTimer(Max_Time, Min_Time)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent=new Intent(AddEditPasswordsActivity.this, MainActivity.class);
                intent.putExtra("pin_flag",true);
                startActivity(intent);

//                finishAffinity();
//                startActivity(new Intent(AddEditPasswordsActivity.this,MainActivity.class));
                /*finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }
        }.start();
//////////////////////////////////////////////



        editTextTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return AddEditPasswordsActivity.super.onTouchEvent(event);
            }
        });
        editTextName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return false;
            }
        });
        editTextUser_ID.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return false;
            }
        });
        editTextPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return false;
            }
        });
        editTextNotes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                    return false;
                }
                return false;
            }
        });


        AllRelativeLayoutEditText=findViewById(R.id.reltive);
        // enabling action bar app icon and behaving it as toggle button
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        //retrive shared pref
        Intent intent=getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle(getResources().getString(R.string.edit_pass));
            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextName.setText(intent.getStringExtra(EXTRA_NAME));
            editTextUser_ID.setText(intent.getStringExtra(EXTRA_USER_ID));
            editTextPassword.setText(intent.getStringExtra(EXTRA_PASSWORD));
            editTextNotes.setText(intent.getStringExtra(EXTRA_NOTES));



        }else{
            setTitle(getResources().getString(R.string.add_pass));
        }
//        countDownTimer = new CountDownTimer(Max_Time, Min_Time) {
//
//            public void onTick(long millisUntilFinished) {
//                //TODO: Do something every second
//
//            }
//
//            public void onFinish() {
//                Intent intent=new Intent(AddEditPasswordsActivity.this, MainActivity.class);
//
//                intent.putExtra("pin_flag",true);
//                startActivity(intent);
//
////                finishAffinity();
////                System.exit(0);
//                /*finish();
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_HOME);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);*/
//
//
//            }
//        }.start();





    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            countDownTimer.cancel();
            countDownTimer.start();
            return false;
        }
        return super.onTouchEvent(event);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
            countDownTimer.cancel();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        countDownTimer.cancel();
        countDownTimer.start();

        super.onResume();
    }
}